package ProductState;

import Products.Product;

/**
 * @author akhmaali, bondani1
 *
 * The class represents state for product
 */
public abstract class State {

    protected Product product;

    /**
     * Constructor for state creation
     *
     * @param product product
     */
    public State(Product product) {
        this.product = product;
    }

    public abstract void changeToNextState();


}
