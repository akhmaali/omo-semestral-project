package ProductState;

import Products.Product;

/**
 * @author akhmaali, bondani1
 *
 * The class represents prepared state for product
 */
public class Prepared extends State {

    /**
     * Creates Prepared state
     *
     * @param product product
     */
    public Prepared(Product product) {
        super(product);
    }

    /**
     * changes the state of product to Delivered
     */
    @Override
    public void changeToNextState() {
        product.setState(new Delivered(product));
    }
}
