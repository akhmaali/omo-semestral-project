package ProductState;

import Products.Product;

/**
 * @author akhmaali, bondani1
 *
 * The class represents delivered state for product
 */
public class Delivered extends State {

    /**
     * Creates Delivered state
     *
     * @param product product
     */
    public Delivered(Product product) {
        super(product);
    }

    /**
     * changes the state of product to Sold
     */
    @Override
    public void changeToNextState() {
        product.setState(new Sold(product));
    }
}
