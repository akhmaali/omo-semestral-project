package ProductState;

import Products.Product;

/**
 * @author akhmaali, bondani1
 *
 * The class represents grown state for product
 */
public class Grown extends State {

    /**
     * Creates Grown state
     *
     * @param product product
     */
    public Grown(Product product) {
        super(product);
    }

    /**
     * changes the state of product to Prepared
     */
    @Override
    public void changeToNextState() {
        product.setState(new Prepared(product));
    }
}
