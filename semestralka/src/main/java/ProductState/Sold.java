package ProductState;

import Products.Product;

/**
 * @author akhmaali, bondani1
 *
 * The class represents sold state for product
 */
public class Sold extends State {

    /**
     * Creates Sold state
     *
     * @param product product
     */
    public Sold(Product product) {
        super(product);
    }

    @Override
    public void changeToNextState() {

    }
}
