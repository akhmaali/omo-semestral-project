package ProductState;

import Products.Product;

/**
 * @author akhmaali, bondani1
 *
 * The class represents created state for product
 */
public class Created extends State {

    /**
     * Creates Created state
     *
     * @param product
     */
    public Created(Product product) {
        super(product);
    }

    /**
     * changes the state of product to Grown
     */
    @Override
    public void changeToNextState() {
        product.setState(new Grown(product));
    }
}
