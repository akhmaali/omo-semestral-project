package Exceptions;

public class BadChannelException extends  RuntimeException {

    public BadChannelException(String partName, String className) {
        super(String.format("Participant [%s] can't use [%s]", partName, className));
    }
}
