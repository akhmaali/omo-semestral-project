package Channels;

import BlockChain.Block;
import Participants.AbstractParticipant;
import Products.Product;

/**
 * @author akhmaali, bondani1
 *
 * The class represents payment channel. Singleton pattern applied
 */
public class PaymentChannel {

    private static PaymentChannel instance;

    private PaymentChannel() {
    }

    /**
     * Constructor for single instance channel
     *
     * @return channel created or that already was created
     */
    public static PaymentChannel getInstance() {
        if (instance == null) instance = new PaymentChannel();
        return instance;
    }

    /**
     * Illustrates payment
     * Notifies all participants about the created block
     *
     * @param buyer buyer
     * @param seller seller
     * @param product product to be bought
     */
    public void buy(AbstractParticipant buyer, AbstractParticipant seller, Product product) {
        seller.setBalance(seller.getBalance() + product.getPrice());
        buyer.setBalance(buyer.getBalance() - product.getPrice());

        String transaction = "'" + buyer.getClass().toString().substring(19) + "'" + ", method: 'buy()', parameters: {buyer: " + buyer.getName() + " seller: " + seller.getName() + ", product: " + product.toString().substring(21) + ", price: " + product.getPrice() + "}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size() - 1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);
        product.setPrice(product.getPrice() * 1.1);
    }
}
