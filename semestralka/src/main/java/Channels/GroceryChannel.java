package Channels;

import Exceptions.BadChannelException;
import Participants.AbstractParticipant;
import Products.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * @author akhmaali, bondani1
 *
 * The class represents grocery channel. Singleton pattern applied
 */
public class GroceryChannel extends Channel {

    private static GroceryChannel instance;
    private List<Request> requests = new ArrayList<>();
    private List<Product> products = new ArrayList<>();
    private List<AbstractParticipant> forbidenParticipants = new ArrayList<>();


    private GroceryChannel() {
    }

    /**
     * Constructor for single instance channel
     *
     * @return channel created or that already was created
     */
    public static GroceryChannel getInstance() {
        if (instance == null) instance = new GroceryChannel();
        return instance;
    }


    public void getFruits(Request request) {
        requests.add(request);
    }


    public void getVegetables(Request request) {
        requests.add(request);
    }

    /**
     * Adds product to channel(list of products)
     *
     * @param product product
     */
    public void send(Product product) {
        if (forbidenParticipants.contains(product.getLastParticipant()))
            throw new BadChannelException(product.getLastParticipant().getName(), "Groccery channel");
        getProducts().add(product);
    }


    public List<Request> getRequests() {
        return requests;
    }


    public void setRequests(List<Request> requests) {
        this.requests = requests;
    }


    public List<Product> getProducts() {
        return products;
    }


    public void setProducts(List<Product> products) {
        this.products = products;
    }

    /**
     * Adds participant to the list of forbidden participants
     *
     * @param participant participant
     */
    public void forbid(AbstractParticipant participant) {
        forbidenParticipants.add(participant);
    }
}
