package Channels;

import Participants.AbstractParticipant;

import java.util.ArrayList;
import java.util.List;

/**
 * @author akhmaali, bondani1
 *
 * The class represents common channel. Singleton pattern applied
 */
public class CommonChannel extends Channel{

    private static CommonChannel instance;
    private List<AbstractParticipant> participants = new ArrayList<>();

    private CommonChannel(){};

    /**
     * Constructor for single instance channel
     *
     * @return channel created or that already was created
     */
    public static CommonChannel getInstance() {
        if (instance == null) instance = new CommonChannel();
        return instance;
    }

    public List<AbstractParticipant> getParticipants() {
        return participants;
    }


    public void setParticipants(List<AbstractParticipant> participants) {
        this.participants = participants;
    }

    /**
     * Adds participants to channel
     *
     * @param participant participant
     */
    public  void add(AbstractParticipant participant) {
        participants.add(participant);
    }
}
