package Channels;

import Exceptions.BadChannelException;
import Participants.AbstractParticipant;
import Products.Product;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author akhmaali, bondani1
 *
 * The class represents maet channel. Singleton pattern applied
 */
public class MeatChannel extends Channel{

    private static MeatChannel instance;
    private List<Request> requests = new ArrayList<>();
    private List<Product> products = new ArrayList<>();
    private List<AbstractParticipant>  forbidenParticipants = new ArrayList<>();


    private MeatChannel() {
    }

    /**
     * Constructor for single instance channel
     *
     * @return channel created or that already was created
     */
    public static MeatChannel getInstance() {
        instance = Optional.ofNullable(instance).orElse(new MeatChannel());
        return instance;
    }


    public void getChicken(Request request) {
        requests.add(request);

    }

    public void getPork(Request request) {
        requests.add(request);
    }


    public void getBeef(Request request) {
        requests.add(request);
    }

    /**
     * Adds product to channel(list of products)
     *
     * @param product product
     */
    public void send(Product product) {
        if (forbidenParticipants.contains(product.getLastParticipant())) throw new BadChannelException(product.getLastParticipant().getName(), "Meat channel");
        getProducts().add(product);
    }

    public List<Request> getRequests() {
        return requests;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    /**
     * Adds participant to the list of forbidden participants
     *
     * @param participant participant
     */
    public void forbid(AbstractParticipant participant) {
        forbidenParticipants.add(participant);
    }
}
