package Channels;

import Participants.Customer;

/**
 * @author akhmaali, bondani1
 *
 * The class represents request
 */
public class Request {

    private Customer customer;
    private String type;
    private int weight;

    /**
     * Creates request for customer
     *
     * @param customer customer
     * @param type type of product
     * @param weight weight of product
     */
    public Request(Customer customer, String type, int weight) {
        this.customer = customer;
        this.type = type;
        this.weight = weight;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
