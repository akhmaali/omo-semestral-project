package Products;

import Channels.Request;
import ProductState.Created;

/**
 * @author akhmaali, bondani1
 *
 * The class represents vegetables
 */
public class Vegetables extends Product{

    /**
     * Creates vegetables, sets price per kilogram
     *
     * @param request request to be assigned to vegetables
     */
    public Vegetables(Request request) {
        super(request);
        setPricePERkg(50);
        setPrice(getPricePERkg()*request.getWeight());
    }

}
