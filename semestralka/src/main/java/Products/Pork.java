package Products;

import Channels.Request;
import ProductState.Created;

/**
 * @author akhmaali, bondani1
 *
 * The class represents pork
 */
public class Pork extends Product{

    /**
     * Creates pork, sets price per kilogram
     *
     * @param request request to be assigned to pork
     */
    public Pork(Request request) {
        super(request);
        setPricePERkg(150);
        setPrice(getPricePERkg()*request.getWeight());
    }
}
