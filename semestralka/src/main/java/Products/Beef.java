package Products;

import Channels.Request;
import ProductState.Created;

/**
 * @author akhmaali, bondani1
 *
 * The class represents beef
 */
public class Beef extends Product {

    /**
     * Creates beef, sets price per kilogram
     *
     * @param request request to be assigned to beef
     */
    public Beef(Request request) {
        super(request);
        setPricePERkg(180);
        setPrice(getPricePERkg()*request.getWeight());
    }
}
