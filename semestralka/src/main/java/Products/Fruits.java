package Products;

import Channels.Request;
import ProductState.Created;

/**
 * @author akhmaali, bondani1
 *
 * The class represents fruits
 */
public class Fruits extends Product {

    /**
     * Creates fruits, sets price per kilogram
     *
     * @param request request to be assigned to fruits
     */
    public Fruits(Request request) {
        super(request);
        setPricePERkg(70);
        setPrice(getPricePERkg()*request.getWeight());
    }
}
