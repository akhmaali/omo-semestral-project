package Products;

import Channels.Request;
import ProductState.Created;

/**
 * @author akhmaali, bondani1
 *
 * The class represents chicken
 */
public class Chicken extends Product {

    /**
     * Creates chicken, sets price per kilogram
     *
     * @param request request to be assigned to chicken
     */
    public Chicken(Request request) {
        super(request);
        setPricePERkg(120);
        setPrice(getPricePERkg()*request.getWeight());
    }
}
