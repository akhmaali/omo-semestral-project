package Products;

import BlockChain.Chain;
import Channels.Request;
import Participants.AbstractParticipant;
import Participants.Customer;
import ProductState.Created;
import ProductState.State;

/**
 * @author akhmaali, bondani1
 *
 * The class represents product
 */
public abstract class Product {

    public static int globalId;
    private int id;
    private Chain chain;
    private State state;
    private Customer owner;
    private int weight;
    private int pricePERkg = 0;
    private double price;
    private AbstractParticipant lastParticipant;
    private Request request;

    /**
     * Constructor for creating products
     *
     * @param request request to be assigned to product
     */
    public Product(Request request) {
        this.request = request;
        setGlobalId(getGlobalId()+1);
        setId(getGlobalId());
        setWeight(request.getWeight());
        setState(new Created(this));
    }


    public State getState() {
        return state;
    }


    public void setState(State state) {
        this.state = state;
    }


    public int getWeight() {
        return weight;
    }


    public void setWeight(int weight) {
        this.weight = weight;
    }


    public void setChain(Chain chain) {
        this.chain = chain;
    }


    public Chain getChain() {
        return chain;
    }


    public double getPrice() {
        return price;
    }


    public void setPricePERkg(int pricePERkg) {
        this.pricePERkg = pricePERkg;
    }


    public int getPricePERkg() {
        return pricePERkg;
    }


    public void setPrice(double price) {
        this.price = price;
    }


    public AbstractParticipant getLastParticipant() {
        return lastParticipant;
    }


    public void setLastParticipant(AbstractParticipant lastParticipant) {
        this.lastParticipant = lastParticipant;
    }


    public Request getRequest() {
        return request;
    }


    public void setRequest(Request request) {
        this.request = request;
    }


    public static int getGlobalId() {
        return globalId;
    }


    public static void setGlobalId(int globalId) {
        Product.globalId = globalId;
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return string representation of product
     */
    @Override
    public String toString() {
        return "type: "+ getClass().toString()+ ", id: " +  getId() + ", weight:" + getWeight();
    }
}
