package GroceryStrategy;

import BlockChain.Block;
import BlockChain.Chain;
import Channels.GroceryChannel;
import Channels.Request;
import Participants.GroceryFarmer;
import Participants.GroceryProcessor;
import Products.Fruits;
import Products.Product;

/**
 * @author akhmaali, bondani1
 *
 * The class represents fruits strategy
 */
public class FruitsStrategy extends GroceryStrategy {

    /**
     * Creates fruits strategy for grocery farmer
     *
     * @param farmer grocery farmer
     */
    public FruitsStrategy(GroceryFarmer farmer) {
        super(farmer);
    }

    /**
     * Creates fruits strategy for grocery processor
     *
     * @param processor grocery processor
     */
    public FruitsStrategy(GroceryProcessor processor) {
        super(processor);
    }

    /**
     * Creates vegetables, notifies all participants about the created block
     * Hands vegetables over to grow() method
     *
     * @param request request
     */
    @Override
    public void makeGrocery(Request request) {
        Fruits fruit = new Fruits(request);
        farmer.storeProduct(fruit);
        Chain chain = new Chain();
        fruit.setChain(chain);
        String transaction = "'GrocceryFarmer: " + farmer.getName()+ ", method: 'makeGroccery()', parameters: {product: " + fruit.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        chain.addBlock(block);
        chain.notifyAll(block);
        grow(fruit);
    }

    /**
     * Grows vegetables, sets state to Grown, notifies all participants about the created block
     * Sends product to grocery channel
     *
     * @param product product
     */
    @Override
    public void grow(Product product) {
        product.getState().changeToNextState();
        farmer.removeProduct(product);
        String transaction = "'GrocceryFarmer: " + farmer.getName()+ ", method: 'grow()', parameters: {product: " + product.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size()-1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);
        GroceryChannel channel = GroceryChannel.getInstance();
        product.setLastParticipant(farmer);
        channel.send(product);
    }

    /**
     * Prepares vegetables, sets state to Prepared, notifies all participants about the created block
     * Sends product to grocery channel
     *
     * @param product product
     */
    @Override
    public void prepareGrocery(Product product) {
        product.getState().changeToNextState();
        processor.removeProduct(product);
        String transaction = "'GrocceryProcessor: " + processor.getName()+ ", method: 'makeGroccery()', parameters: {product: " + product.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size()-1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);
        GroceryChannel channel = GroceryChannel.getInstance();
        product.setLastParticipant(processor);
        channel.send(product);
    }
}
