package GroceryStrategy;

import Channels.Request;
import Participants.*;
import Products.Product;

/**
 * The class represents grocery strategy
 */
public abstract class GroceryStrategy {
    protected GroceryFarmer farmer;
    protected GroceryProcessor processor;

    /**
     * Constructor to create grocery strategy for grocery farmer
     *
     * @param farmer grocery farmer
     */
    public GroceryStrategy(GroceryFarmer farmer) {
        this.farmer = farmer;
    }

    /**
     * Constructor to create grocery strategy for grocery processor
     *
     * @param processor grocery processor
     */
    public GroceryStrategy(GroceryProcessor processor) {
        this.processor = processor;
    }

    /**
     * Makes grocery
     *
     * @param request request
     */
    public abstract void makeGrocery(Request request);

    /**
     * Prepares grocery
     *
     * @param product product
     */
    public abstract void prepareGrocery(Product product);

    /**
     * Grows grocery
     *
     * @param product product
     */
    public abstract void grow(Product product);

}
