package GroceryStrategy;

import BlockChain.Block;
import BlockChain.Chain;
import Channels.GroceryChannel;
import Channels.Request;
import Participants.GroceryFarmer;
import Participants.GroceryProcessor;
import Products.Product;
import Products.Vegetables;

/**
 * @author akhmaali, bondani1
 *
 * The class represents fruits strategy
 */
public class VegetablesStrategy extends GroceryStrategy {

    /**
     * Creates vegetables strategy for grocery farmer
     *
     * @param farmer grocery farmer
     */
    public VegetablesStrategy(GroceryFarmer farmer) {
        super(farmer);
    }

    /**
     * Creates vegetables strategy for grocery processor
     *
     * @param processor grocery processor
     */
    public VegetablesStrategy(GroceryProcessor processor) {
        super(processor);
    }

    /**
     * Creates grocery, notifies all participants about the created block
     * Hands vegetable over to grow() method
     *
     * @param request request
     */
    @Override
    public void makeGrocery(Request request) {
        Vegetables vegetable = new Vegetables(request);
        farmer.storeProduct(vegetable);
        Chain chain = new Chain();
        vegetable.setChain(chain);
        String transaction = "'GrocceryFarmer: " + farmer.getName()+ ", method: 'makeGroccery()', parameters: {product: " + vegetable.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        chain.addBlock(block);
        chain.notifyAll(block);
        grow(vegetable);
    }

    @Override
    public void grow(Product product) {
        product.getState().changeToNextState();
        farmer.removeProduct(product);
        String transaction = "'GrocceryFarmer: " + farmer.getName()+ ", method: 'grow()', parameters: {product: " + product.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size()-1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);
        GroceryChannel channel = GroceryChannel.getInstance();
        product.setLastParticipant(farmer);
        channel.send(product);
    }

    @Override
    public void prepareGrocery(Product product) {
        product.getState().changeToNextState();
        processor.removeProduct(product);
        String transaction = "'GrocceryProcessor: " + processor.getName()+ ", method: 'prepareGroccery()', parameters: {product: " + product.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size()-1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);
        GroceryChannel channel = GroceryChannel.getInstance();
        product.setLastParticipant(processor);
        channel.send(product);
    }
}
