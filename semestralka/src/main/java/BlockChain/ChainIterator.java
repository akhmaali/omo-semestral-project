package BlockChain;

/**
 * @author akhmaali, bondani1
 *
 * The class represents chain iterator
 */
public class ChainIterator implements Iterator {

    private Chain chain;
    private Block current;
    private boolean onStart = true;

    /**
     * Crates chain iterator
     *
     * @param chain chain
     */
    public ChainIterator(Chain chain) {
        this.chain = chain;
        for (Block block: chain.getBlocks()) if (block.getPrevHash()==0) current=block;
    }

    /**
     * Check whether chain has next block
     *
     * @return true/false
     */
    public boolean hasNext() {
        if (onStart) {
            return true;
        }
        for (Block block: chain.getBlocks()) if (block.getPrevHash()==current.getHash()) return true;
        return false;
    }

    /**
     *
     * @return next block of a chain
     */
    public Block next() {
        if (onStart) {
            onStart = false;
            return current;
        }
        for (Block block: chain.getBlocks()) if (block.getPrevHash()==current.getHash()) {
            current = block;
            break;
        }
        return current;
    }
}
