package BlockChain;

/**
 * @author akhmaali, bondani1
 *
 * The class represents single block of blockchain
 */
public class Block {

    private String data;
    private int hash;
    private int prevHash;
    private boolean wasHacked = false;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
        this.hash =  data.hashCode();
        this.wasHacked = true;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    public int getPrevHash() {
        return prevHash;
    }

    public void setPrevHash(int prevHash) {
        this.prevHash = prevHash;
    }


    public boolean wasHacked() {
        return wasHacked;
    }


    public void setWasHacked(boolean wasHacked) {
        this.wasHacked = wasHacked;
    }

    /**
     * Creates block
     *
     * @param transaction represents data in block and is used to create hash
     * @return created block
     */
    public static Block createBlock(String transaction) {
        Block block = new Block();
        block.data  = transaction;
        block.hash = transaction.hashCode();

        return block;
    }
}
