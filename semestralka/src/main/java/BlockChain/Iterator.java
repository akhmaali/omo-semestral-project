package BlockChain;

public interface Iterator {
    public boolean hasNext();
    public Block next();
}
