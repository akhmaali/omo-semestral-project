package BlockChain;

public interface ChainContainer {
    public ChainIterator getIterator();
}
