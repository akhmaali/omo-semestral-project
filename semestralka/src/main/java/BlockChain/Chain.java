package BlockChain;

import Channels.CommonChannel;
import Participants.AbstractParticipant;
import Products.Product;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author akhmaali, bondani1
 *
 * The class represnts chain of blockchain
 */
public class Chain implements ChainContainer {


    private Product product;
    List<Block> blocks = new ArrayList<>();

    /**
     * Part of Observer design pattern
     * Notifies all participants to add block to their chain
     *
     * @param block block to be added to chain
     */
    public void notifyAll(Block block) {
        CommonChannel commonChannel = CommonChannel.getInstance();
        for (AbstractParticipant participant : commonChannel.getParticipants()) {
            participant.update(block, participant);
        }
    }

    /**
     * Adds block to list of blocks in chain
     *
     * @param block block
     */
    public void addBlock(Block block) {
        blocks.add(block);
    }

    public List<Block> getBlocks() {
        return blocks;
    }

    public void setBlocks(List<Block> blocks) {
        this.blocks = blocks;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * Creates chain iterator
     *
     * @return iterator
     */
    public ChainIterator getIterator() {
        return new ChainIterator(this);
    }

    /**
     * Generates chain report
     */
    public void makeReport() {
        try {
            PrintWriter outputStream = new PrintWriter("ChainReport" + Math.random() * 100 + ".txt");
            outputStream.println("Chain Report\n");
            ChainIterator it = getIterator();
            while (it.hasNext()) {
                Block block = it.next();
                if (block.wasHacked()) outputStream.println("This block was hacked");
                outputStream.println("Transaction: " + block.getData());
                outputStream.println("Hash: " + block.getHash());
                if (block.getPrevHash() != 0) outputStream.println("Previous Hash: " + block.getPrevHash() + "\n");
                else outputStream.println("\n");
            }
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
