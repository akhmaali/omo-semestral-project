package Participants;

public interface ParticipantsForVis {
    public void accept(ParticipantsVisitor participantsVisitor);
}
