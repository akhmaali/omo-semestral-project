package Participants;

import Channels.GroceryChannel;
import Channels.MeatChannel;
import Channels.PaymentChannel;
import Channels.Request;
import GroceryStrategy.FruitsStrategy;
import GroceryStrategy.VegetablesStrategy;
import MeatStrategy.BeefStrategy;
import MeatStrategy.ChickenStrategy;
import MeatStrategy.PorkStrategy;
import ProductState.Delivered;
import ProductState.Grown;
import ProductState.Prepared;
import Products.*;

/**
 * @author akhmaali, bondani1
 *
 * The class represents visitor implementation
 */
public class ParticipantsVisitorImpl implements ParticipantsVisitor {

    /**
     * If the list of requests is not empty in meat channel, goes through the list and checks the type of request
     * Depending on the type of request, the corresponding strategy is set on the meat farmer
     * Subsequently calls method to make meat
     *
     * @param meatFarmer meat farmer
     */
    @Override
    public void visit(MeatFarmer meatFarmer) {
        MeatChannel channel = MeatChannel.getInstance();
        if (!channel.getRequests().isEmpty()) {
            for (int i = 0; i < channel.getRequests().size(); i++) {
                Request request = channel.getRequests().get((i));
                if (request.getType().equals("chicken")) {
                    meatFarmer.setStrategy(new ChickenStrategy(meatFarmer));
                    channel.getRequests().remove(request);
                    meatFarmer.getStrategy().makeMeat(request);
                    break;
                } else if (request.getType().equals("pork")) {
                    meatFarmer.setStrategy(new PorkStrategy(meatFarmer));
                    channel.getRequests().remove(request);
                    meatFarmer.getStrategy().makeMeat(request);
                    break;
                } else if (request.getType().equals("beef")) {
                    meatFarmer.setStrategy(new BeefStrategy(meatFarmer));
                    channel.getRequests().remove(request);
                    meatFarmer.getStrategy().makeMeat(request);
                    break;
                }
            }
        }
    }

    /**
     * If the list of products in meat channel is not empty, goes through the list and checks the state of products
     * If the state of product is Grown, depending on the type of product the corresponding strategy is set
     * Processor buys the product from farmer and subsequently prepares meat
     *
     * @param meatProcessor meat processor
     */
    @Override
    public void visit(MeatProcessor meatProcessor) {
        MeatChannel channel = MeatChannel.getInstance();
        if (!channel.getProducts().isEmpty()) {
            for (int i=0;i<channel.getProducts().size();i++) {
                Product product  = channel.getProducts().get((i));
                //maybe define name of state (final)
                if (product.getState().getClass().equals(Grown.class)) {
                    if (product.getClass().equals(Chicken.class)) {
                        PaymentChannel paymentChannel = PaymentChannel.getInstance();
                        paymentChannel.buy(meatProcessor, product.getLastParticipant(), product);
                        channel.getProducts().remove(product);
                        meatProcessor.storeProduct(product);
                        meatProcessor.setStrategy(new ChickenStrategy(meatProcessor));
                        meatProcessor.getStrategy().prepareMeat(product);
                        break;
                    }
                    else if (product.getClass().equals(Pork.class)) {
                        PaymentChannel paymentChannel = PaymentChannel.getInstance();
                        paymentChannel.buy(meatProcessor, product.getLastParticipant(), product);
                        channel.getProducts().remove(product);
                        meatProcessor.storeProduct(product);
                        meatProcessor.setStrategy(new PorkStrategy(meatProcessor));
                        meatProcessor.getStrategy().prepareMeat(product);
                        break;
                    }
                    else if (product.getClass().equals(Beef.class)) {
                        PaymentChannel paymentChannel = PaymentChannel.getInstance();
                        paymentChannel.buy(meatProcessor, product.getLastParticipant(), product);
                        channel.getProducts().remove(product);
                        meatProcessor.storeProduct(product);
                        meatProcessor.setStrategy(new BeefStrategy(meatProcessor));
                        meatProcessor.getStrategy().prepareMeat(product);
                        break;
                    }
                }
            }
        }
    }

    /**
     * If the list of requests is not empty in grocery channel, goes through the list and checks the type of request
     * Depending on the type of request, the corresponding strategy is set on the grocery farmer
     * Subsequently calls method to make grocery
     *
     * @param groceryFarmer grocery farmer
     */
    @Override
    public void visit(GroceryFarmer groceryFarmer) {
        GroceryChannel channel = GroceryChannel.getInstance();
        if (!channel.getRequests().isEmpty()) {
            for (int i = 0; i < channel.getRequests().size(); i++) {
                Request request = channel.getRequests().get((i));
                if (request.getType().equals("fruit")) {
                    groceryFarmer.setStrategy(new FruitsStrategy(groceryFarmer));
                    channel.getRequests().remove(request);
                    groceryFarmer.getStrategy().makeGrocery(request);
                    break;
                } else if (request.getType().equals("vegetable")) {
                    groceryFarmer.setStrategy(new VegetablesStrategy(groceryFarmer));
                    channel.getRequests().remove(request);
                    groceryFarmer.getStrategy().makeGrocery(request);
                    break;
                }

            }
        }
    }

    /**
     * If the list of products in grocery channel is not empty, goes through the list and checks the state of products
     * If the state of product is Grown, depending on the type of product the corresponding strategy is set
     * Processor buys the product from farmer and subsequently prepares grocery
     *
     * @param groceryProcessor grocery processor
     */
    @Override
    public void visit(GroceryProcessor groceryProcessor) {
        GroceryChannel channel = GroceryChannel.getInstance();
        if (!channel.getProducts().isEmpty()) {
            for (int i=0;i<channel.getProducts().size();i++) {
                Product product  = channel.getProducts().get((i));
                if (product.getState().getClass().equals(Grown.class)) {
                    if (product.getClass().equals(Fruits.class)) {
                        PaymentChannel paymentChannel = PaymentChannel.getInstance();
                        paymentChannel.buy(groceryProcessor, product.getLastParticipant(), product);
                        channel.getProducts().remove(product);
                        groceryProcessor.storeProduct(product);
                        groceryProcessor.setStrategy(new FruitsStrategy(groceryProcessor));
                        groceryProcessor.getStrategy().prepareGrocery(product);
                        break;
                    } else if (product.getClass().equals(Vegetables.class)) {
                        PaymentChannel paymentChannel = PaymentChannel.getInstance();
                        paymentChannel.buy(groceryProcessor, product.getLastParticipant(), product);
                        channel.getProducts().remove(product);
                        groceryProcessor.storeProduct(product);
                        groceryProcessor.setStrategy(new VegetablesStrategy(groceryProcessor));
                        groceryProcessor.getStrategy().prepareGrocery(product);
                        break;
                    }
                }
            }
        }
    }

    /**
     * If the list of products in meat channel or grocery channel is not empty,
     * goes through the list of products in channel and checks the state of products
     * If the state of product is Prepared, distributor buys the product prom processor and
     * subsequently delivers the product
     *
     * @param distributor distributor
     */
    @Override
    public void visit(Distributor distributor) {
        MeatChannel channel = MeatChannel.getInstance();
        if (!channel.getProducts().isEmpty()) {
            for (int i=0;i<channel.getProducts().size();i++) {
                Product product  = channel.getProducts().get((i));
                if (product.getState().getClass().equals(Prepared.class)) {
                    PaymentChannel paymentChannel = PaymentChannel.getInstance();
                    paymentChannel.buy(distributor, product.getLastParticipant(), product);
                    channel.getProducts().remove(product);
                    distributor.storeProduct(product);
                    distributor.deliver(product);
                    return;
                }
            }
        }

        GroceryChannel channel2 = GroceryChannel.getInstance();
        if (!channel2.getProducts().isEmpty()) {
            for (int i=0;i<channel2.getProducts().size();i++) {
                Product product  = channel2.getProducts().get((i));
                if (product.getState().getClass().equals(Prepared.class)) {
                    PaymentChannel paymentChannel = PaymentChannel.getInstance();
                    paymentChannel.buy(distributor, product.getLastParticipant(), product);
                    channel2.getProducts().remove(product);
                    distributor.storeProduct(product);
                    distributor.deliver(product);
                    return;
                }
            }
        }
    }

    /**
     * If the list of products in meat channel or grocery channel is not empty,
     * goes through the list of products in channel and checks the state of products
     * If the state of product is Delivered, retailer buys the product prom processor and
     * subsequently sells the product
     *
     * @param retailer retailer
     */
    @Override
    public void visit(Retailer retailer) {
        MeatChannel channel = MeatChannel.getInstance();
        if (!channel.getProducts().isEmpty()) {
            for (int i=0;i<channel.getProducts().size();i++) {
                Product product  = channel.getProducts().get((i));
                if (product.getState().getClass().equals(Delivered.class)) {
                    PaymentChannel paymentChannel = PaymentChannel.getInstance();
                    paymentChannel.buy(retailer, product.getLastParticipant(), product);
                    channel.getProducts().remove(product);
                    retailer.storeProduct(product);
                    retailer.sell(product);
                    return;
                }
            }
        }

        GroceryChannel channel2 = GroceryChannel.getInstance();
        if (!channel2.getProducts().isEmpty()) {
            for (int i=0;i<channel2.getProducts().size();i++) {
                Product product  = channel2.getProducts().get((i));
                if (product.getState().getClass().equals(Delivered.class)) {
                    PaymentChannel paymentChannel = PaymentChannel.getInstance();
                    paymentChannel.buy(retailer, product.getLastParticipant(), product);
                    channel2.getProducts().remove(product);
                    retailer.storeProduct(product);
                    retailer.sell(product);
                    return;
                }
            }
        }
    }

    /**
     * If the list of products in meat channel or grocery channel is not empty
     * and the customer's list of request is not empty,
     * goes through the list of products in channel and checks whether the customer contains product's request.
     * Subsequently if match occurred customer buys and receives the product
     *
     * @param customer customer
     */
    @Override
    public void visit(Customer customer) {
        MeatChannel channel = MeatChannel.getInstance();
        if (!channel.getProducts().isEmpty() && !customer.getRequests().isEmpty()) {
            for (int i=0;i<channel.getProducts().size();i++) {
                Product product  = channel.getProducts().get((i));
                if (customer.getRequests().contains(product.getRequest())) {
                    PaymentChannel paymentChannel = PaymentChannel.getInstance();
                    paymentChannel.buy(customer, product.getLastParticipant(), product);
                    customer.getRequests().remove(product.getRequest());
                    channel.getProducts().remove(product);
                    customer.receive(product);
                }
            }
        }

        GroceryChannel channel2 = GroceryChannel.getInstance();
        if (!channel2.getProducts().isEmpty() && !customer.getRequests().isEmpty()) {
            for (int i=0;i<channel2.getProducts().size();i++) {
                Product product  = channel2.getProducts().get((i));
                if (customer.getRequests().contains(product.getRequest())) {
                    PaymentChannel paymentChannel = PaymentChannel.getInstance();
                    paymentChannel.buy(customer, product.getLastParticipant(), product);
                    customer.getRequests().remove(product.getRequest());
                    channel2.getProducts().remove(product);
                    customer.receive(product);
                }
            }
        }
    }
}
