package Participants;

/**
 * @author akhmaali, bondani1
 *
 * The class represents processor
 */
public abstract class AbstractProcessor extends AbstractParticipant {

    public AbstractProcessor(String name) {
        super(name);
    }

}
