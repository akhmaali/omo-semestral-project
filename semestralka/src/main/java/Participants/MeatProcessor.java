package Participants;

import MeatStrategy.MeatStrategy;


/**
 * @author akhmaali, bondani1
 *
 * The class represents meat processor, who is reponsible for opeating with meat
 */
public class MeatProcessor extends AbstractProcessor{

    private MeatStrategy strategy;

    /**
     * Creates meat processor
     *
     * @param name processor's name
     */
    public MeatProcessor(String name) {
        super(name);
    }

    public MeatStrategy getStrategy() {
        return strategy;
    }


    public void setStrategy(MeatStrategy strategy) {
        this.strategy = strategy;
    }

    /**
     * Part of Visitor design pattern implementation
     * Accepts visitor and calls visit() method on it, which accepts this instance of meat processor
     * visit() method contains main operations
     *
     * @param participantsVisitor visitor
     */
    @Override
    public void accept(ParticipantsVisitor participantsVisitor) {
        participantsVisitor.visit(this);
    }
}

