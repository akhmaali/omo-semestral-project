package Participants;

public interface ParticipantsVisitor {
    public void visit(MeatFarmer meatFarmer);
    public void visit(MeatProcessor meatProcessor);
    public void visit(GroceryFarmer groceryFarmer);
    public void visit(GroceryProcessor groceryProcessor);
    public void visit(Distributor distributor);
    public void visit(Retailer retailer);
    public void visit(Customer customer);
}
