package Participants;

import BlockChain.Block;
import Channels.GroceryChannel;
import Channels.MeatChannel;
import Products.*;

/**
 * @author akhmaali, bondani1
 *
 * The class represents retailer, who sells products
 */
public class Retailer extends AbstractParticipant{

    /**
     * Creates retailer
     *
     * @param name retailer's name
     */
    public Retailer(String name) {
        super(name);
    }

    /**
     * Represents sell operation
     * Notifies all participants about the created block
     *
     * @param product product to be sold
     */
    public void sell(Product product) {
        product.getState().changeToNextState();
        removeProduct(product);
        String transaction = "'Retailer: " + this.getName()+ ", method: 'sell()', parameters: {product: " + product.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size()-1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);
        if (product.getClass().equals(Chicken.class)||product.getClass().equals(Beef.class)||product.getClass().equals(Pork.class)) {
            MeatChannel channel = MeatChannel.getInstance();
            product.setLastParticipant(this);
            channel.getProducts().add(product);
        }
        if (product.getClass().equals(Fruits.class)||product.getClass().equals(Vegetables.class)) {
            GroceryChannel channel = GroceryChannel.getInstance();
            product.setLastParticipant(this);
            channel.getProducts().add(product);
        }
    }

    /**
     * Part of Visitor design pattern implementation
     * Accepts visitor and calls visit() method on it, which accepts this instance of retailer
     * visit() method contains main operations
     *
     * @param participantsVisitor visitor
     */
    @Override
    public void accept(ParticipantsVisitor participantsVisitor) {
        participantsVisitor.visit(this);
    }
}
