package Participants;

import BlockChain.Block;
import BlockChain.Chain;
import Products.Product;
import java.util.ArrayList;
import java.util.List;

/**
 * @author akhmaali,bondani1
 *
 * The class represents asbtract participant which is inherited by corresponding subclasses(parties/participants)
 */
public abstract class AbstractParticipant implements ParticipantsForVis{

    private static int globalId = 0;
    private int id;
    private String name;
    private Warehouse warehouse;
    private List<Chain> chains = new ArrayList<>();
    private double balance = 10000;

    /**
     * Constructor to create parties(subclasses)
     * Sets id, global id and creates warehouse
     *
     * @param name name of party(participant)
     */
    public AbstractParticipant(String name) {
        this.name = name;
        setGlobalId(getGlobalId() + 1);
        setId(getGlobalId());
        setWarehouse(new Warehouse());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void storeProduct(Product product) {
        warehouse.add(product);
    }

    public void removeProduct(Product product) {
        warehouse.remove(product);
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public List<Chain> getChains() {
        return chains;
    }

    public void setChains(List<Chain> chains) {
        this.chains = chains;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public static int getGlobalId() {
        return globalId;
    }

    public static void setGlobalId(int globalId) {
        AbstractParticipant.globalId = globalId;
    }

    /**
     * Part of Visitor design pattern implementation
     *
     * @param participantsVisitor visitor
     */
    public abstract void accept(ParticipantsVisitor participantsVisitor);

    /**
     * Part of Observer design pattern implementation
     * If the previous block(prev hash) does not exist, new chain is created
     * If the block's previous hash is equal to the hash of the last block in the chain, the block is added to that chain
     *
     * @param block block to be added to chain
     * @param participant participant, whose chain has to be appended by the block
     */
    public void update(Block block, AbstractParticipant participant) {
        if (block.getPrevHash() == 0) {
            Chain chain2 = new Chain();
            participant.getChains().add(chain2);
            chain2.addBlock(block);
        }
        for (int i = 0; i < participant.getChains().size(); i++) {
            Chain chain = participant.getChains().get(i);
            if (chain.getBlocks().get(chain.getBlocks().size() - 1).getHash() == block.getPrevHash()) {
                chain.addBlock(block);
            }
        }
    }
}
