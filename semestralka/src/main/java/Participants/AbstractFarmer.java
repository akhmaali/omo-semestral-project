package Participants;

/**
 * @author akhmaali, bondani1
 *
 * The class represents farmer
 */
public abstract class AbstractFarmer extends AbstractParticipant {

    public AbstractFarmer(String name) {
        super(name);
    }
}
