package Participants;

import BlockChain.Block;
import Channels.GroceryChannel;
import Channels.MeatChannel;
import Channels.Request;
import Products.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * @author akhmaali, bondani1
 *
 * The class represents customer, who is able to make requests for products
 */
public class Customer extends AbstractParticipant{

    private List<Request> requests = new ArrayList<>();

    /**
     * Creates customer
     *
     * @param name customer's name
     */
    public Customer(String name) {
        super(name);
    }

    /**
     * Request for chicken
     * Sends request to meat channel
     *
     * @param weight desired weight of chicken
     */
    public void getChicken(int weight) {
        Request request = new Request(this, "chicken", weight);
        requests.add(request);
        MeatChannel channel = MeatChannel.getInstance();
        channel.getChicken(request);
    }

    /**
     * Request for pork
     * Sends request to meat channel
     *
     * @param weight desired weight of pork
     */
    public void getPork(int weight) {
        Request request = new Request(this, "pork", weight);
        requests.add(request);
        MeatChannel channel = MeatChannel.getInstance();
        channel.getChicken(request);
    }

    /**
     * Request for beef
     * Sends request to meat channel
     *
     * @param weight desired weight of beef
     */
    public void getBeef(int weight) {
        Request request = new Request(this, "beef", weight);
        requests.add(request);
        MeatChannel channel = MeatChannel.getInstance();
        channel.getChicken(request);;
    }

    /**
     * Request for fruits
     * Sends request to grocery channel
     *
     * @param weight desired weight of fruits
     */
    public void getFruits(int weight) {
        Request request = new Request(this, "fruit", weight);
        requests.add(request);
        GroceryChannel channel = GroceryChannel.getInstance();
        channel.getFruits(request);
    }

    /**
     * Request for vegetables
     * Sends request to grocery channel
     *
     * @param weight desired weight of vegetables
     */
    public void getVegetables(int weight) {
        Request request = new Request(this, "vegetable", weight);
        requests.add(request);
        GroceryChannel channel = GroceryChannel.getInstance();
        channel.getVegetables(request);
    }

    /**
     * Receives product and stores it to warehouse
     * Notifies all participants about the created block
     *
     * @param product product to be received
     */
    public void receive(Product product) {
        storeProduct(product);
        String transaction = "'Customer: " + this.getName()+ ", method: 'receive()', parameters: {product: " + product.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size() - 1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);

    }

    public List<Request> getRequests() {
        return requests;
    }

    /**
     * Part of Visitor design pattern implementation
     * Accepts visitor and calls visit() method on it, which accepts this instance of customer
     * visit() method contains main operations
     *
     * @param participantsVisitor visitor
     */
    @Override
    public void accept(ParticipantsVisitor participantsVisitor) {
        participantsVisitor.visit(this);
    }
}
