package Participants;

import Products.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * @author akhmaali, bondani1
 *
 * The class represents warehouse, where products can be stored
 */
public class Warehouse {

    private List<Product> products = new ArrayList<>();


    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    /**
     * Adds product to the list of products in warehouse
     *
     * @param product product to be added
     */
    public void add(Product product) {
        products.add(product);
    }

    /**
     * Removes product to the list of products in warehouse
     *
     * @param product product to be removed
     */
    public void remove(Product product) {
        products.remove(product);
    }

}
