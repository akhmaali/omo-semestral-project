package Participants;


import GroceryStrategy.*;

/**
 * @author akhmaali, bondani1
 *
 * The class represents grocery processor, who is reponsible for opeating with groceries
 */
public class GroceryProcessor extends AbstractProcessor {

    private GroceryStrategy strategy;

    /**
     * Creates grocery processor
     *
     * @param name processor's name
     */
    public GroceryProcessor(String name) {
        super(name);
    }

    public GroceryStrategy getStrategy() {
        return strategy;
    }


    public void setStrategy(GroceryStrategy strategy) {
        this.strategy = strategy;
    }

    /**
     * Part of Visitor design pattern implementation
     * Accepts visitor and calls visit() method on it, which accepts this instance of grocery processor
     * visit() method contains main operations
     *
     * @param participantsVisitor visitor
     */
    @Override
    public void accept(ParticipantsVisitor participantsVisitor) {
        participantsVisitor.visit(this);
    }
}