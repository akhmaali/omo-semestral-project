package Participants;

import GroceryStrategy.*;

/**
 * @author akhmaali, bondani1
 *
 * The class represents grocery farmer, who is reponsible for opeating with groceries
 */
public class GroceryFarmer extends AbstractFarmer{

    private GroceryStrategy strategy;

    /**
     * Creates grocery farmer
     *
     * @param name farmer's name
     */
    public GroceryFarmer(String name) {
        super(name);
    }

    public GroceryStrategy getStrategy() {
        return strategy;
    }


    public void setStrategy(GroceryStrategy strategy) {
        this.strategy = strategy;
    }

    /**
     * Part of Visitor design pattern implementation
     * Accepts visitor and calls visit() method on it, which accepts this instance of grocery farmer
     * visit() method contains main operations
     *
     * @param participantsVisitor visitor
     */
    @Override
    public void accept(ParticipantsVisitor participantsVisitor) {
        participantsVisitor.visit(this);
    }
}
