package Participants;

import BlockChain.Block;
import Channels.GroceryChannel;
import Channels.MeatChannel;
import Products.*;

/**
 * @author akhmaali, bondani1
 *
 * The class represents distributor, who is responsible for delivering products
 */
public class Distributor extends AbstractParticipant{

    /**
     * Creates distributor
     *
     * @param name distributor's name
     */
    public Distributor(String name) {
        super(name);
    }

    /**
     * Sends product to corresponding channel
     * Notifies all participants about the created block
     *
     * @param product product to be delivered
     */
    public void deliver(Product product) {
        product.getState().changeToNextState();
        removeProduct(product);
        String transaction = "'Distributor: " + this.getName()+ ", method: 'deliver()', parameters: {product: " + product.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size() - 1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);
        if (product.getClass().equals(Chicken.class)||product.getClass().equals(Beef.class)||product.getClass().equals(Pork.class)) {
            MeatChannel channel = MeatChannel.getInstance();
            product.setLastParticipant(this);
            channel.send(product);
        }
        if (product.getClass().equals(Fruits.class)||product.getClass().equals(Vegetables.class)) {
            GroceryChannel channel = GroceryChannel.getInstance();
            product.setLastParticipant(this);
            channel.send(product);
        }
    }

    /**
     * Part of Visitor design pattern implementation
     * Accepts visitor and calls visit() method on it, which accepts this instance of distributor
     * visit() method contains main operations
     *
     * @param participantsVisitor visitor
     */
    @Override
    public void accept(ParticipantsVisitor participantsVisitor) {
        participantsVisitor.visit(this);
    }
}
