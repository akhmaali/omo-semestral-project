package Participants;

import MeatStrategy.*;

/**
 * @author akhmaali, bondani1
 *
 * The class represents meat farmer, who is reponsible for opeating with meat
 */
public class MeatFarmer extends AbstractFarmer{

    private MeatStrategy strategy;

    /**
     * Creates meat farmer
     *
     * @param name farmer's name
     */
    public MeatFarmer(String name) {
        super(name);
    }

    public MeatStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(MeatStrategy strategy) {
        this.strategy = strategy;
    }

    /**
     * Part of Visitor design pattern implementation
     * Accepts visitor and calls visit() method on it, which accepts this instance of meat farmer
     * visit() method contains main operations
     *
     * @param participantsVisitor visitor
     */
    @Override
    public void accept(ParticipantsVisitor participantsVisitor) {
        participantsVisitor.visit(this);
    }
}
