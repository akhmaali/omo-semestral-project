package MeatStrategy;

import Channels.Request;
import Participants.MeatFarmer;
import Participants.MeatProcessor;
import Products.Product;

/**
 * The class represents meat strategy
 */
public abstract class MeatStrategy {

    protected MeatFarmer farmer;
    protected MeatProcessor processor;

    /**
     * Constructor to create meat strategy for meat farmer
     *
     * @param farmer meat farmer
     */
    public MeatStrategy(MeatFarmer farmer) {
        this.farmer = farmer;
    }

    /**
     * Constructor to create meat strategy for meat processor
     *
     * @param processor meat processor
     */
    public MeatStrategy(MeatProcessor processor) {
        this.processor = processor;
    }

    /**
     * Makes meat
     *
     * @param request request
     */
    public abstract void makeMeat(Request request);

    /**
     * Prepares meat
     *
     * @param product product
     */
    public abstract void prepareMeat(Product product);

    /**
     * Grows meat
     *
     * @param product product
     */
    public abstract void grow(Product product);
}
