package MeatStrategy;

import BlockChain.Block;
import BlockChain.Chain;
import Channels.MeatChannel;
import Channels.Request;
import Participants.MeatFarmer;
import Participants.MeatProcessor;
import Products.Beef;
import Products.Product;

/**
 * @author akhmaali, bondani1
 *
 * The class represents beef strategy
 */
public class BeefStrategy extends MeatStrategy {

    /**
     * Creates beef strategy for meat farmer
     *
     * @param farmer meat farmer
     */
    public BeefStrategy(MeatFarmer farmer) {
        super(farmer);
    }

    /**
     * Creates beef strategy for meat processor
     *
     * @param processor meat processor
     */
    public BeefStrategy(MeatProcessor processor) {
        super(processor);
    }

    /**
     * Creates beef, notifies all participants about the created block
     * Hands beef over to grow() method
     *
     * @param request request
     */
    @Override
    public void makeMeat(Request request) {
        Beef beef = new Beef(request);
        farmer.storeProduct(beef);
        Chain chain = new Chain();
        beef.setChain(chain);
        String transaction = "'MeatFarmer: " + farmer.getName()+ ", method: 'makeMeat()', parameters: {product: " + beef.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        chain.addBlock(block);
        chain.notifyAll(block);
        grow(beef);
    }

    /**
     * Grows beef, sets state to Grown, notifies all participants about the created block
     * Sends product to meat channel
     *
     * @param product product
     */
    @Override
    public void grow(Product product) {
        product.getState().changeToNextState();
        farmer.removeProduct(product);
        String transaction = "'MeatFarmer: " + farmer.getName()+ ", method: 'grow()', parameters: {product: " + product.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size()-1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);
        MeatChannel channel = MeatChannel.getInstance();
        product.setLastParticipant(farmer);
        channel.send(product);
    }

    /**
     * Prepares beef, sets state to Prepared, notifies all participants about the created block
     * Sends product to meat channel
     * @param product product
     */
    @Override
    public void prepareMeat(Product product) {
        product.getState().changeToNextState();
        processor.removeProduct(product);
        String transaction = "'MeatProcessor: " + processor.getName()+ ", method: 'prepareMeat()', parameters: {product: " + product.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size()-1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);
        MeatChannel channel = MeatChannel.getInstance();
        product.setLastParticipant(processor);
        channel.send(product);
    }
}
