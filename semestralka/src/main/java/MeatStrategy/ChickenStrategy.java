package MeatStrategy;

import BlockChain.Block;
import BlockChain.Chain;
import Channels.MeatChannel;
import Channels.Request;
import Participants.MeatFarmer;
import Participants.MeatProcessor;
import Products.Chicken;
import Products.Product;

/**
 * @author akhmaali, bondani1
 *
 * The class represents chicken strategy
 */
public class ChickenStrategy extends MeatStrategy {

    /**
     * Creates chicken strategy for meat farmer
     *
     * @param farmer meat farmer
     */
    public ChickenStrategy(MeatFarmer farmer) {
        super(farmer);
    }

    /**
     * Creates chicken strategy for meat processor
     *
     * @param processor meat processor
     */
    public ChickenStrategy(MeatProcessor processor) {
        super(processor);
    }

    /**
     * Creates chicken, notifies all participants about the created block
     * Hands chicken over to grow() method
     *
     * @param request request
     */
    @Override
    public void makeMeat(Request  request) {
        Chicken chicken = new Chicken(request);
        farmer.storeProduct(chicken);
        Chain chain = new Chain();
        chicken.setChain(chain);
        String transaction = "'MeatFarmer: " + farmer.getName()+ ", method: 'makeMeat()', parameters: {product: " + chicken.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        chain.addBlock(block);
        chain.notifyAll(block);
        grow(chicken);
    }

    /**
     * Grows chicken, sets state to Grown, notifies all participants about the created block
     * Sends product to meat channel
     *
     * @param product product
     */
    @Override
    public void grow(Product product) {
        product.getState().changeToNextState();
        farmer.removeProduct(product);
        String transaction = "'MeatFarmer: " + farmer.getName()+ ", method: 'grow()', parameters: {product: " + product.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size()-1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);
        MeatChannel channel = MeatChannel.getInstance();
        product.setLastParticipant(farmer);
        channel.send(product);
    }

    /**
     * Prepares chicken, sets state to Prepared, notifies all participants about the created block
     * Sends product to meat channel
     *
     * @param product product
     */
    @Override
    public void prepareMeat(Product product) {
        product.getState().changeToNextState();
        processor.removeProduct(product);
        String transaction = "'MeatProcessor: " + processor.getName()+ ", method: 'prepare()', parameters: {product: " + product.toString().substring(21) +"}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size()-1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);
        MeatChannel channel = MeatChannel.getInstance();
        product.setLastParticipant(processor);
        channel.send(product);
    }
}
