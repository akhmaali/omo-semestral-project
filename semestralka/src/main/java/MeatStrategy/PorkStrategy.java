package MeatStrategy;

import BlockChain.Block;
import BlockChain.Chain;
import Channels.MeatChannel;
import Channels.Request;
import Participants.MeatFarmer;
import Participants.MeatProcessor;
import Products.Pork;
import Products.Product;

/**
 * @author akhmaali, bondani1
 *
 * The class represents pork strategy
 */
public class PorkStrategy extends MeatStrategy {

    /**
     * Creates pork strategy for meat farmer
     *
     * @param farmer meat farmer
     */
    public PorkStrategy(MeatFarmer farmer) {
        super(farmer);
    }

    /**
     * Creates pork strategy for meat processor
     *
     * @param processor meat processor
     */
    public PorkStrategy(MeatProcessor processor) {
        super(processor);
    }

    /**
     * Creates pork, notifies all participants about the created block
     * Hands pork over to grow() method
     *
     * @param request request
     */
    @Override
    public void makeMeat(Request request) {
        Pork pork = new Pork(request);
        farmer.storeProduct(pork);
        Chain chain = new Chain();
        pork.setChain(chain);
        String transaction = "'MeatFarmer: " + farmer.getName() + ", method: 'makeMeat()', parameters: {product: " + pork.toString().substring(21) + "}";
        Block block = Block.createBlock(transaction);
        chain.addBlock(block);
        chain.notifyAll(block);
        grow(pork);
    }

    /**
     * Grows pork, sets state to Grown, notifies all participants about the created block
     * Sends product to meat channel
     *
     * @param product product
     */
    @Override
    public void grow(Product product) {
        product.getState().changeToNextState();
        farmer.removeProduct(product);
        String transaction = "'MeatFarmer: " + farmer.getName() + ", method: 'grow()', parameters: {product: " + product.toString().substring(21) + "}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size() - 1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);
        MeatChannel channel = MeatChannel.getInstance();
        product.setLastParticipant(farmer);
        channel.send(product);

    }

    /**
     * Prepares pork, sets state to Prepared, notifies all participants about the created block
     * Sends product to meat channel
     *
     * @param product product
     */
    @Override
    public void prepareMeat(Product product) {
        product.getState().changeToNextState();
        processor.removeProduct(product);
        String transaction = "'MeatProcessor: " + processor.getName() + ", method: 'prepareMeat()', parameters: {product: " + product.toString().substring(21) + "}";
        Block block = Block.createBlock(transaction);
        block.setPrevHash(product.getChain().getBlocks().get(product.getChain().getBlocks().size() - 1).getHash());
        product.getChain().addBlock(block);
        product.getChain().notifyAll(block);
        MeatChannel channel = MeatChannel.getInstance();
        product.setLastParticipant(processor);
        channel.send(product);

    }
}
