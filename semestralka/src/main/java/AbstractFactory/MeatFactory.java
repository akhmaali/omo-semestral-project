package AbstractFactory;

import Participants.AbstractFarmer;
import Participants.AbstractProcessor;
import Participants.MeatFarmer;
import Participants.MeatProcessor;

/**
 * @author akhmaali, bondani1
 *
 * The class represnts meat factory
 */
public class MeatFactory implements AbstractFactory{

    /**
     * Creates meat farmer
     *
     * @param name name of meat farmer
     * @return created meat farmer
     */
    public AbstractFarmer createFarmer(String name) {
        return new MeatFarmer(name);
    }

    /**
     * Creates meat processor
     *
     * @param name name of meat processor
     * @return created meat processor
     */
    public AbstractProcessor createProcessor(String name) {
        return new MeatProcessor(name);
    }
}
