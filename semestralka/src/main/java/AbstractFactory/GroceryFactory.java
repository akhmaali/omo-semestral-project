package AbstractFactory;

import Participants.*;

/**
 * @author akhmaali, bondani1
 *
 * The class represnts grocery factory
 */
public class GroceryFactory implements AbstractFactory {

    /**
     * Creates grocery farmer
     *
     * @param name name of grocery farmer
     * @return created grocery farmer
     */
    public AbstractFarmer createFarmer(String name) {
        return new GroceryFarmer(name);
    }

    /**
     * Creates grocery processor
     *
     * @param name name of grocery processor
     * @return created grocery processor
     */
    public AbstractProcessor createProcessor(String name) {
        return new GroceryProcessor(name);
    }
}
