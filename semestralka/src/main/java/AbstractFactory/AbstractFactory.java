package AbstractFactory;

import Participants.AbstractFarmer;
import Participants.AbstractProcessor;

public interface AbstractFactory {
    public AbstractFarmer createFarmer(String name);
    public AbstractProcessor createProcessor(String name);
}
