import AbstractFactory.AbstractFactory;
import BlockChain.Block;
import Channels.CommonChannel;
import Channels.GroceryChannel;
import Channels.MeatChannel;
import Participants.*;
import AbstractFactory.*;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;


public class Main {

    private static int counter;

    public static void main(String[] args) {

        Customer customer = new Customer("Ali Akhmadov");
        Customer customer2 = new Customer("Nikita Bondarev");
        AbstractFactory meatFactory = new MeatFactory();
        AbstractFactory groceryFactory = new GroceryFactory();
        AbstractFarmer meatFarmer = meatFactory.createFarmer("MeatFarmInc");
        AbstractFarmer groceryFarmer = groceryFactory.createFarmer("GrocFarmInc");
        AbstractProcessor meatProcessor = meatFactory.createProcessor("TastyMeatProc");
        AbstractProcessor groceryProcessor = groceryFactory.createProcessor("TastyGrassProc");
        Distributor distributor = new Distributor("GoodCar");
        Distributor distributor2 = new Distributor("ReallyGoodCar");
        Retailer retailer = new Retailer("SuperShop");
        Retailer retailer2 = new Retailer("MegaShop");

        MeatChannel meatChannel = MeatChannel.getInstance();
        meatChannel.forbid(groceryFarmer);
        meatChannel.forbid(groceryProcessor);

        GroceryChannel groceryChannel = GroceryChannel.getInstance();
        groceryChannel.forbid(meatFarmer);
        groceryChannel.forbid(meatProcessor);


        CommonChannel commonChannel = CommonChannel.getInstance();
        commonChannel.add(meatFarmer);
        commonChannel.add(groceryFarmer);
        commonChannel.add(meatProcessor);
        commonChannel.add(groceryProcessor);
        commonChannel.add(distributor);
        commonChannel.add(distributor2);
        commonChannel.add(retailer);
        commonChannel.add(retailer2);
        commonChannel.add(customer);
        commonChannel.add(customer2);

        customer.getBeef(10);
        customer.getFruits(10);
        customer.getPork(12);
        customer.getVegetables(10);
        customer.getBeef(12);
        customer2.getVegetables(12);
        customer.getFruits(23);
        customer.getChicken(12);


        for (int i = 0; i < 8; i++) {
            for (AbstractParticipant participant : commonChannel.getParticipants()) {
                participant.accept(new ParticipantsVisitorImpl());
            }
            //makeReport(participants);
        }

        System.out.println(customer.getWarehouse().getProducts());
        System.out.println(customer2.getWarehouse().getProducts());
        System.out.println(customer.getBalance());


        for (Block block : customer.getWarehouse().getProducts().get(0).getChain().getBlocks())
            System.out.println(block.getData());



        //Generating chain report
        // customer.getWarehouse().getProducts().get(0).getChain().makeReport();

    }


    public static void makeReport(List<AbstractParticipant> participants) {
        try {
            PrintWriter outputStream = new PrintWriter("Report" + counter++ + ".txt");
            outputStream.println("Main Report\n");
            for (AbstractParticipant participant : participants) {
                outputStream.println(participant.getName() + ", balance: " + participant.getBalance() + "\n");
            }
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
