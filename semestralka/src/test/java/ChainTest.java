import BlockChain.Block;
import BlockChain.Iterator;
import Channels.CommonChannel;
import Channels.MeatChannel;
import Participants.Customer;
import Participants.MeatFarmer;
import Participants.MeatProcessor;
import Participants.ParticipantsVisitorImpl;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;


public class ChainTest {

    @Test
    public void productChainBuildSuccess() {
        Customer customer = new Customer("test");
        MeatFarmer farmer = new MeatFarmer("test");
        MeatProcessor processor = new MeatProcessor("test");
        customer.getChicken(10);
        farmer.accept(new ParticipantsVisitorImpl());
        processor.accept(new ParticipantsVisitorImpl());
        assertEquals(4, MeatChannel.getInstance().getProducts().get(0).getChain().getBlocks().size());
    }


    @Test
    public void participantChainsBuildSuccess() {
        Customer customer = new Customer("test");
        MeatFarmer farmer = new MeatFarmer("test");
        MeatProcessor processor = new MeatProcessor("test");

        CommonChannel.getInstance().add(customer);
        CommonChannel.getInstance().add(farmer);
        CommonChannel.getInstance().add(processor);

        customer.getChicken(10);
        customer.getPork(10);

        farmer.accept(new ParticipantsVisitorImpl());
        processor.accept(new ParticipantsVisitorImpl());

        farmer.accept(new ParticipantsVisitorImpl());

        assertTrue(farmer.getChains().size() == 2);
        assertTrue(customer.getChains().size() == 2);
        assertTrue(processor.getChains().size() == 2);

        assertTrue(farmer.getChains().get(0).getBlocks().size()==4);
        assertTrue(farmer.getChains().get(1).getBlocks().size()==2);
    }


    @Test
    public void iteratorWorks() {
        Customer customer = new Customer("test");
        MeatFarmer farmer = new MeatFarmer("test");
        MeatProcessor processor = new MeatProcessor("test");

        customer.getPork(10);

        farmer.accept(new ParticipantsVisitorImpl());
        processor.accept(new ParticipantsVisitorImpl());

        List<Block> blocks = new ArrayList<>();
        Iterator it = MeatChannel.getInstance().getProducts().get(0).getChain().getIterator();
        while (it.hasNext()) {
            Block block = it.next();
            blocks.add(block);
        }

        assertEquals(4,blocks.size());
    }


    @Test
    public void changingBlockDataCorruptsChain() {
        Customer customer = new Customer("test");
        MeatFarmer farmer = new MeatFarmer("test");
        MeatProcessor processor = new MeatProcessor("test");

        customer.getPork(10);

        farmer.accept(new ParticipantsVisitorImpl());
        processor.accept(new ParticipantsVisitorImpl());

        MeatChannel.getInstance().getProducts().get(0).getChain().getBlocks().get(0).setData("sdf");

        List<Block> blocks = new ArrayList<>();
        Iterator it = MeatChannel.getInstance().getProducts().get(0).getChain().getIterator();
        while (it.hasNext()) {
            Block block = it.next();
            blocks.add(block);
        }

        assertEquals(1,blocks.size());
    }
}
