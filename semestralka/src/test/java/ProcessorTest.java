import Channels.MeatChannel;
import Participants.*;
import ProductState.Prepared;
import Products.Chicken;
import Products.Product;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.TestCase.assertTrue;


public class ProcessorTest {

    @Test
    public void processMeatIfProductIsPresent() {
        MeatChannel.getInstance().setProducts(new ArrayList<Product>());
        Customer customer = new Customer("test");
        MeatFarmer farmer = new MeatFarmer("test");
        MeatProcessor processor  =  new MeatProcessor("test");
        customer.getChicken(10);
        farmer.accept(new ParticipantsVisitorImpl());
        processor.accept(new ParticipantsVisitorImpl());
        assertTrue(MeatChannel.getInstance().getProducts().size() == 1);
        assertTrue(MeatChannel.getInstance().getProducts().get(0).getState().getClass().equals(Prepared.class));
    }


    @Test
    public void doNothingIfNoProduct() {
        MeatChannel.getInstance().setProducts(new ArrayList<Product>());
        Customer customer = new Customer("test");
        MeatFarmer farmer = new MeatFarmer("test");
        MeatProcessor processor  =  new MeatProcessor("test");
        customer.getChicken(10);
        farmer.accept(new ParticipantsVisitorImpl());
        processor.accept(new ParticipantsVisitorImpl());
        assertTrue(MeatChannel.getInstance().getProducts().size() == 1);

        processor.accept(new ParticipantsVisitorImpl());
        assertTrue(MeatChannel.getInstance().getProducts().size() == 1);
    }

    @Test
    public void doNothingIfRequestOtherType() {
        MeatChannel.getInstance().setProducts(new ArrayList<Product>());
        Customer customer = new Customer("test");
        GroceryFarmer farmer = new GroceryFarmer("test");
        MeatProcessor processor  =  new MeatProcessor("test");
        customer.getVegetables(10);
        farmer.accept(new ParticipantsVisitorImpl());
        processor.accept(new ParticipantsVisitorImpl());
        assertTrue(MeatChannel.getInstance().getProducts().size() == 0);
    }
}
