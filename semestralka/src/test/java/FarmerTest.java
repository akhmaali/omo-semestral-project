import Channels.MeatChannel;
import Participants.Customer;
import Participants.MeatFarmer;
import Participants.ParticipantsVisitorImpl;
import Products.Chicken;
import Products.Product;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.TestCase.assertTrue;


public class FarmerTest {

    @Test
    public void makeMeatifRequestIsPresent() {
        MeatChannel.getInstance().setProducts(new ArrayList<Product>());
        Customer customer = new Customer("test");
        MeatFarmer farmer = new MeatFarmer("test");
        customer.getChicken(10);
        assertTrue(MeatChannel.getInstance().getProducts().size() == 0);
        farmer.accept(new ParticipantsVisitorImpl());
        assertTrue(MeatChannel.getInstance().getProducts().size() == 1);
        assertTrue(MeatChannel.getInstance().getProducts().get(0).getClass() == Chicken.class);
    }


    @Test
    public void doNothingIfNoRequest() {
        MeatChannel.getInstance().setProducts(new ArrayList<Product>());
        MeatFarmer farmer = new MeatFarmer("test");
        farmer.accept(new ParticipantsVisitorImpl());
        assertTrue(MeatChannel.getInstance().getProducts().size() == 0);
    }

    @Test
    public void doNothingIfRequestOtherType() {
        MeatChannel.getInstance().setProducts(new ArrayList<Product>());
        Customer customer = new Customer("test");
        MeatFarmer farmer = new MeatFarmer("test");
        customer.getVegetables(10);
        farmer.accept(new ParticipantsVisitorImpl());
        assertTrue(MeatChannel.getInstance().getProducts().size() == 0);
    }
}
